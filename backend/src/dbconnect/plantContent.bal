import ballerina/jsonutils;
import ballerina/log;
import ballerina/stringutils;
import ballerinax/java.jdbc;

public type PlantDesc record {|
    string plant_id;
    string name;
    string[] properties;
    string imageURL;
|};

public type PlantDB record {
    string plant_id;
    string name;
    string properties;
    string imageURL;
};

public type PlantImg record {|
    string name;
    string fileType;
    byte[] image;
|};

public type PlantContent record {|
    string name;
    byte[] imageContent;
|};

public function insertPlantDesc(jdbc:Client c, PlantDB[] plants) returns json|error {
    string sqlString =
        "INSERT INTO PLANTDESC (PLANTID, NAME, PROPERTIES, IMAGEURL) VALUES (?,?,?,?)";
    var res = plants.map(function (PlantDB plant) returns jdbc:UpdateResult|jdbc:DatabaseError|jdbc:ApplicationError {
        var updateRes = c->update(sqlString, plant.plant_id, plant.name, plant.properties, plant.imageURL);
        return updateRes;
    });
    var e = res.map(function (jdbc:UpdateResult|jdbc:DatabaseError|jdbc:ApplicationError result) returns json {
        if (result is jdbc:UpdateResult) {
            log:printInfo("Status: Data Inserted Successfully");
            return check_db_return(result);
        } else {
            return check_db_return(result);
        }
    });
    json emptyJson = {};
    var results = e.reduce(function (json pr, json ne) returns json {
        map<json> mp = <map<json>>ne;
        if (mp["INFO"] == "Error") {
            return ne;
        }
        return pr;
    }, emptyJson);
    return results;

}

public function selectPlantDesc(jdbc:Client c, string parameters) returns @tainted json {
    json jsonReturnValue = {};
    string sqlString =
        "SELECT * FROM PLANTDESC WHERE name LIKE ? OR FIND_IN_SET(?, properties)";
    var ret = c->select(sqlString, PlantDB, parameters, parameters);
    if (ret is table<PlantDB>) {
        jsonReturnValue = jsonutils:fromTable(ret);
        return jsonReturnValue;
    } else {
        return check_db_return(ret);
    }
}

public function selectPlantAll(jdbc:Client c) returns @tainted (json|error)[]|json {
    json jsonReturnValue = {};
    string sqlString =
        "SELECT * FROM PLANTDESC";
    var ret = c->select(sqlString, PlantDB);
    if (ret is table<PlantDB>) {
        jsonReturnValue = jsonutils:fromTable(ret);
        json[] jsonPlants = <json[]>jsonReturnValue;
        (json|error)[] plantList = jsonPlants.map(function (json jsonPlant) returns json|error {
            map<json> plant = <map<json>>jsonPlant;
            var properties = stringutils:split(plant["properties"].toString(), ",");
            json plantAsObject = {
                id: plant["plant_id"].toString(),
                name: plant["name"].toString(),
                properties: properties,
                imageURL: plant["imageURL"].toString()
            };
            return plantAsObject;
        });
        return plantList;
    } else {
        return check_db_return(ret);
    }
}

public function selectPlantImg(jdbc:Client c, string name, string filetype) returns @tainted json|PlantImg {
    json jsonReturnValue = {};
    string sqlString =
        "SELECT Name,fileType,Image FROM PLANTIMG WHERE name = ? AND fileType = ?";
    DBReturn ret = c->select(sqlString, PlantImg, name, filetype);
    if (ret is table<PlantImg>) {
        if (ret.hasNext()) {
            PlantImg plantObj = ret.getNext();
            return plantObj;
        }
        return check_db_return(string `Could not found file with name ${name}.${filetype}`);
    } else {
        return check_db_return(ret);
    }
}

public function setPlantImg(jdbc:Client c, PlantImg plant) returns json {
    json jsonReturnValue = {};
    var uuid = randUUID().toString();
    string sqlString =
        "INSERT INTO PLANTIMG (plantID, Name, fileType, Image) VALUES (?,?,?,?)";
    var ret = c->update(sqlString, uuid, plant.name, plant.fileType, plant.image);
    if (ret is jdbc:UpdateResult) {
        log:printInfo("Status: Data Inserted Successfully");
        jsonReturnValue = {"id": uuid};
        return jsonReturnValue;
    } else {
        return check_db_return(ret);
    }
}

public function selectPlantContent(jdbc:Client c, string parameters) returns @tainted json|PlantContent {
    json jsonReturnValue = {};
    string sqlString =
        "SELECT NAME,IMAGECONTENT FROM PLANTContent WHERE name = ?";
    var ret = c->select(sqlString, PlantContent, parameters);
    if (ret is table<PlantContent>) {
        if (ret.hasNext()) {
            PlantContent plantObj = ret.getNext();
            return plantObj;
        } else {
            string imageMessage = "No image found with name " + parameters;
            return check_db_return(imageMessage);
        }
    } else {
        return check_db_return(ret);
    }
}

public function setPlantContent(jdbc:Client c, PlantContent plant) returns json {
    json jsonReturnValue = {};
    var uuid = randUUID().toString();
    string sqlString =
        "INSERT INTO PLANTContent (contentId, Name, ImageContent) VALUES (?,?,?)";
    var ret = c->update(sqlString, uuid, plant.name, plant.imageContent);
    if (ret is jdbc:UpdateResult) {
        log:printInfo("Status: Data Inserted Successfully");
        jsonReturnValue = {"id": uuid};
        return jsonReturnValue;
    } else {
        return check_db_return(ret);
    }
}
