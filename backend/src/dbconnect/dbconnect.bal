import ballerina/config;
import ballerina/java;
import ballerina/log;
import ballerina/runtime;
import ballerinax/java.jdbc;

public type DBType jdbc:Client;

public type DBError jdbc:DatabaseError| jdbc:ApplicationError;

public type DBAny DBType|DBError;

public type DBReturn table<record {}>|jdbc:DatabaseError|jdbc:ApplicationError|jdbc:UpdateResult;

string dburl = getEnvValue();

int maxConnect = 10;

DBAny|() jdbcClient = ();

function connectToDB() {
    DBAny|error newConnection = trap new ({
        url: string `jdbc:mysql://${dburl}:3306/MOBILE_COMPUTING`,
        username: "root",
        password: "root",
        dbOptions: {useSSL: false}
    });
    if (newConnection is DBAny) {
        jdbcClient = newConnection;
    }
}

public function checkConnection(int connectCount = 0) returns DBType|json {
    if (jdbcClient is DBType) {
        return <DBType>jdbcClient;
    }
    else {
        int newCount;
        if (connectCount < 10) {
            newCount = connectCount + 1;
        } else {
            json jsonReturnValue = {};
            var e = error("");
            if (jdbcClient is DBAny) {
                e = error(jdbcClient.toString());
            }
            jsonReturnValue = {"Status": "Database connection failed", "Info": "Error"};
            log:printError("Error occurred in data retrieval", err = e);
            return jsonReturnValue;
        }
        connectToDB();
        runtime:sleep(5);
        return checkConnection(newCount);
    }
}

public function randUUID() returns handle = @java:Method {
    name: "randomUUID",
    'class: "java.util.UUID"
} external;

public function getEnvValue() returns (string) {
    string key = <@untainted>"docker" + "." + <@untainted>"dburl";
    var foundConfig = config:getAsString(key, "localhost");
    return foundConfig;
}

public function check_db_return(anydata|DBReturn|error ret) returns json {
    json jsonReturnValue = {};
    if (ret is jdbc:DatabaseError) {
        jsonReturnValue = {"Status": "Data Not Found", "Info": "Error"};
        log:printError("Error occurred in data retrieval", err = ret);
    }
    else if (ret is jdbc:ApplicationError) {
        jsonReturnValue = {"Status": "Wrong type Found", "Info": "Error"};
        var e = error(ret.toString());
        log:printError("Error occurred in data retrieval", err = e);
    }
    else if (ret is error) {
        jsonReturnValue = {"Status": "Wrong type Found", "Info": ret.toString()};
        log:printError("Error occurred in data retrieval", err = ret);
    }
    else {
        jsonReturnValue = {"Status": "Successfull", "Info": ret.toString()};
        log:printInfo("Successfull data retrieval");
    }
    return jsonReturnValue;
}
