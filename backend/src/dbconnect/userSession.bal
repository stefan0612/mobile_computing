import ballerina/jsonutils;
import ballerina/log;
import ballerinax/java.jdbc;

public type ModelPosition record {|
    float _x;
    float _y;
    float _z;
    float _scale;
    float _rotate;
|};

public type UserModels record {|
    string _modelName;
    ModelPosition _modelPosition;
|};

public type UserSession record {|
    string id;
    string name;
    string date;
    UserModels[] models;
    json pos;
    boolean inCloud;
|};

function parseUserSession((json|error) input) returns json|error {
    if (input is error) {
        log:printInfo(("session i error"));
        return input;
    }
    map<json> session = <map<json>>input;
    json models = check session["models"].toJsonString().fromJsonString();
    json pos = check session["pos"].toJsonString().fromJsonString();
    string timeFormatting = session["date"].toString();
    boolean inCloud = check boolean.constructFrom(session["inCloud"]);
    json convertedSession = {id: session["id"], name: session["name"], date: timeFormatting, models: models, pos: pos, inCloud: inCloud};
    return convertedSession;
}

public function selectSessionAll(jdbc:Client c) returns @tainted (json|error)[]|json {
    json jsonReturnValue = {};
    string sqlString =
        "SELECT userID as id, name, date, models, pos, inCloud FROM USERSESSION";
    var ret = c->select(sqlString, UserSession);
    if (ret is table<UserSession>) {
        var convertRet = jsonutils:fromTable(ret);
        if (convertRet is json[]) {
            return convertRet.map(parseUserSession);
        } else {
            log:printInfo(("user session error" + convertRet.toString()));
            return check_db_return(ret);
        }
    } else {
        return check_db_return(ret);
    }
}

public function selectSession(jdbc:Client c, string name) returns @tainted json|error {
    json jsonReturnValue = {};
    string sqlString =
        "SELECT * FROM USERSESSION WHERE name = ?";
    var ret = c->select(sqlString, UserSession, name);
    if (ret is table<UserSession>) {
        json|error sessionObj = jsonutils:fromTable(ret);
        if (sessionObj is json[]) {
            if (sessionObj.length() == 1) {
                return sessionObj.map(parseUserSession).pop();
            } else {
                string message = string `session with name: ${name} not found`;
                return check_db_return(message);
            }
        } else {
            return check_db_return(sessionObj);
        }
    } else {
        return check_db_return(ret);
    }
}

public function deleteSession(jdbc:Client c, string name) returns @tainted json {
    json jsonReturnValue = {};
    string sqlString =
        "DELETE FROM USERSESSION WHERE name = ?";
    var ret = c->update(sqlString, name);
    log:printInfo(ret.toString());
    return check_db_return(ret);
}

public function setSession(jdbc:Client c, (UserSession|error)[] sessions) returns json|error {
    string sqlString =
        "REPLACE INTO USERSESSION (userID, NAME, date, models, pos, inCloud) VALUES (?,?,?,?,?, ?)";
    (DBReturn|error)[] res = sessions.map(function (UserSession|error session) returns DBReturn|error {
        if (session is UserSession) {
            var modelJson = check json.constructFrom(session.models);
            var updateRes = c->update(sqlString, session.id, session.name, session.date, modelJson.toJsonString(), session.pos.toJsonString(), session.inCloud);
            return updateRes;
        }
        return <error>session;
    });
    var e = res.map(function (DBReturn|error result) returns json {
        if (result is jdbc:UpdateResult) {
            log:printInfo("Status: Data Inserted Successfully");
            return check_db_return(result);
        } else {
            return check_db_return(result);
        }
    });
    json emptyJson = {};
    var results = e.reduce(function (json pr, json ne) returns json {
        map<json> mp = <map<json>>ne;
        if (mp["INFO"] == "Error") {
            return ne;
        }
        return pr;
    }, emptyJson);
    return results;

}
