import ballerina/http;
import ballerina/log;
//import ballerina/stringutils;
import mobilecomputing/dbconnect as db;

@http:ServiceConfig {
    basePath: "/user/session"
}
service plant on httpListener {

    @http:ResourceConfig {
        methods: ["GET"],
        path: "/"
    }
    resource function getAll(http:Caller caller, http:Request request) {
        http:Response response = new;
        var conn = db:checkConnection();
        if (conn is json) {
            response.setPayload(<@untainted>conn);
        }
        var res = db:selectSessionAll(<db:DBType>conn);
        if (res is json) {
            response.setPayload(<@untainted>res);
        } else {
            json[] userSessionString = [];
            res.forEach(function ((error|json) user) {
                if (user is json) {
                    userSessionString.push(user);
                }
            });
            response.setPayload(<@untainted>userSessionString);
        }
        var result = caller->respond(response);
        if (result is error) {
            log:printError("Error sending response", result);
        }
    }

    @http:ResourceConfig {
        methods: ["GET"],
        path: "/{name}"
    }
    resource function getSingle(http:Caller caller, http:Request request, string name) {
        http:Response response = new;
        var conn = db:checkConnection();
        if (conn is json) {
            response.setPayload(<@untainted>conn);
        }
        var res = db:selectSession(<db:DBType>conn, name);
        if (res is json) {
            response.setPayload(<@untainted>res);
        }
        var result = caller->respond(response);
        if (result is error) {
            log:printError("Error sending response", result);
        }
    }

    @http:ResourceConfig {
        methods: ["DELETE"],
        path: "/{name}"
    }
    resource function delSingle(http:Caller caller, http:Request request, string name) {
        http:Response response = new;
        var conn = db:checkConnection();
        if (conn is json) {
            response.setPayload(<@untainted>conn);
        }
        var res = db:deleteSession(<db:DBType>conn, name);
        response.setPayload(<@untainted>res);
        var result = caller->respond(response);
        if (result is error) {
            log:printError("Error sending response", result);
        }
    }

    @http:ResourceConfig {
        methods: ["POST"],
        path: "/",
        consumes: ["application/json"]
    }
    resource function setSession(http:Caller caller, http:Request request) {
        http:Response response = new;
        var conn = db:checkConnection();
        if (conn is json) {
            response.setPayload(<@untainted>conn);
        }
        var payload = request.getJsonPayload();
        if (payload is json) {
            if (payload is json[]) {
                (db:UserSession|error)[] sessions = payload.map(function (json eachSession) returns db:UserSession|error {
                    return db:UserSession.constructFrom(eachSession);
                });
                var n = sessions.forEach(function (db:UserSession|error i) {
                    if (i is error) {
                        response.statusCode = 500;
                        return;
                    }
                });
                if (sessions.length() > 0) {
                    var res = db:setSession(<db:DBType>conn, sessions);
                    if (res is json) {
                        response.setPayload(<@untainted>res);
                    } else {
                        response.statusCode = 500;
                    }
                }
            } else {
                db:UserSession|error session = db:UserSession.constructFrom(payload);
                if (session is db:UserSession) {
                    var res = db:setSession(<db:DBType>conn, [session]);
                    if (res is json) {
                        response.setPayload(<@untainted>res);
                    } else {
                        response.statusCode = 500;
                    }
                }
            }
        } else {
            response.statusCode = 406;
        }
        var result = caller->respond(response);
        if (result is error) {
            log:printError("Error sending response", result);
        }
    }
}
