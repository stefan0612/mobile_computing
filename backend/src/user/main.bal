import ballerina/docker;
import ballerina/http;
import ballerina/log;
import mobilecomputing/dbconnect as db;

@docker:Config {
    name: "plant-ar-backend",
    tag: "v1.0",
    cmd: "CMD sleep 7 && ls -lah . && ls -lah ./conf/ && java -jar ${APP} --b7a.http.accesslog.console=true"
}

@docker:CopyFiles {
    files: [
            {sourceFile: "./src/user/resources/ballerina.conf", target: "/home/ballerina/conf/ballerina.conf", isBallerinaConf: true}
        ]
}

@docker:Expose {}
listener http:Listener httpListener = new (8080);

public function main() {
    var dbConnection = db:checkConnection();
}

@http:ServiceConfig {
    basePath: "/"
}
service simple on httpListener {

    @http:ResourceConfig {
        methods: ["GET"],
        path: "/data"
    }
    resource function data(http:Caller caller, http:Request request) {
        http:Response response = new;
        json jsonReturnValue = {};
        var payload = db:getEnvValue();
        jsonReturnValue = {"Status": payload};
        response.setPayload(<@untainted>jsonReturnValue);
        var result = caller->respond(response);
        if (result is error) {
            log:printError("Error sending response", result);
        }
    }
}
