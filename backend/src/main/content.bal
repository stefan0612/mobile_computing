import ballerina/http;
import ballerina/log;
import ballerina/stringutils;
import mobilecomputing/dbconnect as db;

function hasImgFiletyp(string nameofImg) returns string[] {
    if (stringutils:contains(nameofImg, ".") == false) {
        return [nameofImg, "jpg"];
    }
    var replace = stringutils:replaceAll(nameofImg, "\\.", " ");
    var splitName = stringutils:split(replace, " ");
    if (splitName.length() != 2) {
        return [nameofImg, "jpg"];
    }
    return splitName;
}

@http:ServiceConfig {
    basePath: "/content"
}
service plant on httpListener {

    @http:ResourceConfig {
        methods: ["GET"],
        path: "/desc/"
    }
    resource function getAll(http:Caller caller, http:Request request) {
        http:Response response = new;
        var conn = db:checkConnection();
        if (conn is json) {
            response.setPayload(<@untainted>conn);
        }
        var res = db:selectPlantAll(<db:DBType>conn);
        if (res is json) {
            response.setPayload(<@untainted>res);
        } else {
            json[] plantDesAsString = [];
            res.forEach(function ((error|json) plant) {
                if (plant is json) {
                    plantDesAsString.push(plant);
                }
            });
            response.setPayload(<@untainted>plantDesAsString);
        }
        var result = caller->respond(response);
        if (result is error) {
            log:printError("Error sending response", result);
        }
    }

    @http:ResourceConfig {
        methods: ["POST"],
        path: "/desc",
        consumes: ["application/json"],
        produces: ["application/json"]
    }
    resource function setPlant(http:Caller caller, http:Request request) {
        http:Response response = new;
        var payload = request.getJsonPayload();
        var conn = db:checkConnection();
        if (conn is json) {
            response.setPayload(<@untainted>conn);
        }
        if (payload is json) {
            map<json> plantDesc = <map<json>>payload;
            db:PlantDB[] plantList = [
                    {
                        plant_id: db:randUUID().toString(),
                        name: plantDesc["name"].toString(),
                        properties: plantDesc["properties"].toString(),
                        imageURL: plantDesc["imageURL"].toString()
                    }
                ];
            var res = db:insertPlantDesc(<db:DBType>conn, plantList);
            if (res is json) {
                response.setPayload(<@untainted>res);
            }
        } else {
            response.statusCode = 406;
        }
        var result = caller->respond(response);
        if (result is error) {
            log:printError("Error sending response", result);
        }
    }

    @http:ResourceConfig {
        methods: ["GET"],
        path: "/desc/{properties}"
    }
    resource function getDesc(http:Caller caller, http:Request request, string properties) {
        http:Response response = new;
        var conn = db:checkConnection();
        if (conn is json) {
            response.setPayload(<@untainted>conn);
        }
        var res = db:selectPlantDesc(<db:DBType>conn, properties);
        response.setPayload(<@untainted>res);
        var result = caller->respond(response);
        if (result is error) {
            log:printError("Error sending response", result);
        }
    }



    @http:ResourceConfig {
        methods: ["GET"],
        path: "/img/{name}"
    }
    resource function getImg(http:Caller caller, http:Request request, string name) {
        http:Response response = new;
        var conn = db:checkConnection();
        if (conn is json) {
            response.setPayload(<@untainted>conn);
        }
        string[] nameSplit = hasImgFiletyp(name);
        var res = db:selectPlantImg(<db:DBType>conn, nameSplit[0], nameSplit[1]);
        if (res is db:PlantImg) {
            response.setPayload(<@untainted>res.image);
        } else {
            response.statusCode = 404;
            response.setPayload(<@untainted>res);
        }
        var result = caller->respond(response);
        if (result is error) {
            log:printError("Error sending response", result);
        }
    }

    @http:ResourceConfig {
        methods: ["POST"],
        path: "/img/{name}",
        produces: ["application/json"]
    }
    resource function setObjectImg(http:Caller caller, http:Request request, string name) {
        http:Response response = new;
        var conn = db:checkConnection();
        if (conn is json) {
            response.setPayload(<@untainted>conn);
        }
        var payload = request.getBinaryPayload();
        var replace = stringutils:replaceAll(name, "\\.", " ");
        var splitName = stringutils:split(replace, " ");
        if (payload is byte[]) {
            var plantObj = {
                name: splitName[0],
                fileType: splitName[1],
                image: payload
            };
            var res = db:setPlantImg(<db:DBType>conn, plantObj);
            response.setPayload(<@untainted>res);
        } else {
            response.statusCode = 406;
        }
        var result = caller->respond(response);
        if (result is error) {
            log:printError("Error sending response", result);
        }
    }

    @http:ResourceConfig {
        methods: ["GET"],
        path: "/object/{properties}"
    }
    resource function getObject(http:Caller caller, http:Request request, string properties) {
        http:Response response = new;
        var conn = db:checkConnection();
        if (conn is json) {
            response.setPayload(<@untainted>conn);
        }
        var res = db:selectPlantContent(<db:DBType>conn, properties);
        if (res is db:PlantContent) {
            response.setPayload(<@untainted>res.imageContent);

        } else {
            response.setPayload(<@untainted>res);
        }
        var result = caller->respond(response);
        if (result is error) {
            log:printError("Error sending response", result);
        }
    }

    @http:ResourceConfig {
        methods: ["POST"],
        path: "/object/{name}",
        produces: ["application/json"]
    }
    resource function setObjectModel(http:Caller caller, http:Request request, string name) {
        http:Response response = new;
        var conn = db:checkConnection();
        if (conn is json) {
            response.setPayload(<@untainted>conn);
        }
        var payload = request.getBinaryPayload();
        if (payload is byte[]) {
            var plantObj = {
                name: name,
                imageContent: payload
            };
            var res = db:setPlantContent(<db:DBType>conn, plantObj);
            response.setPayload(<@untainted>res);
        } else {
            response.statusCode = 406;
        }
        var result = caller->respond(response);
        if (result is error) {
            log:printError("Error sending response", result);
        }
    }
}
