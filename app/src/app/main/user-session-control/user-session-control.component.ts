import { ArSessionComponent } from "./../../ar-session/ar-session.component";
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { ObservableArray } from 'tns-core-modules/data/observable-array';
import { ARModelPosition } from '~/app/model/arsession/ARModelPosition';
import { LocalDbService } from '~/app/service/local-db/local-db.service';
import { ArSessionService } from "~/app/service/ar-session/ar-session.service";
import { ARListItem } from "~/app/model/arsession/ARListItem";

@Component({
  selector: 'app-user-session-control',
  templateUrl: './user-session-control.component.html',
  styleUrls: ['./user-session-control.component.css']
})
export class UserSessionControlComponent implements OnInit {

  private _sessionList: ObservableArray<ARListItem> = new ObservableArray();

  public get sessionList(): ObservableArray<ARListItem> {
    return this._sessionList;
  }

  constructor(
    private arSession: ArSessionService,
    private routerExtensions: RouterExtensions,
    private _changeDetectionRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.initDataItems();
  }

  private initDataItems() {
    this.arSession.getARSessionName()
      .then(r => {
        this._sessionList = new ObservableArray<ARListItem>(r);
      })
    this._changeDetectionRef.detectChanges();
  }

  del(session: ARModelPosition) {
    this.arSession.delARSession(session.name).then(() => {
      this.arSession.setStartSession(undefined)
      this.initDataItems()
    })
  }

  start(session: ARModelPosition) {
    this.arSession.setStartSession(session.name)
    this.routerExtensions.navigate(["ar"])
  }

  goBack(): void {
    this.routerExtensions.back();
  }

  onPullToRefreshInitiated(args) {
    const listView = args.object;
    this.sync()
      .then(() => listView.notifyPullToRefreshFinished())
      .catch(() => listView.notifyPullToRefreshFinished())
  }

  async sync() {
    return this.arSession.syncBackend()
      .then(res => {
        console.log("Sync successfully triggered")
        this.initDataItems();
      })
      .catch(err => {
        console.log("Udate Session on sync Error: " + err);
      })
  }
}
