import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { CommonModule } from "../common/common.module";
import { NativeScriptUIAutoCompleteTextViewModule } from "nativescript-ui-autocomplete/angular";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { MainRoutingModule } from "./main-routing.module";
import { MainComponent } from "./main.component";
import { BackendPlantPickerComponent } from './backend-plant-picker/backend-plant-picker.component';
import { PlantListService } from "../service/plant-list/plant-list.service";
import { NativeScriptHttpClientModule } from "nativescript-angular";
import { UserSessionControlComponent } from './user-session-control/user-session-control.component';

@NgModule({
    imports: [
        NativeScriptUIListViewModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule,
        NativeScriptUIAutoCompleteTextViewModule,
        MainRoutingModule,
        CommonModule,
    ],
    declarations: [
        MainComponent,
        BackendPlantPickerComponent,
        UserSessionControlComponent
    ],
    providers: [
        PlantListService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class MainModule { }
