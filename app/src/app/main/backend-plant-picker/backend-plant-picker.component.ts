import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { PlantListService } from '~/app/service/plant-list/plant-list.service';
import { ObservableArray } from "tns-core-modules/data/observable-array";
import { RouterExtensions } from 'nativescript-angular/router';
import { PlantDesc } from '~/app/model/plant/PlantDesc';
import { ChangeDetectorRef } from "@angular/core";
import { ListViewEventData } from "nativescript-ui-listview";
import { Downloader } from 'nativescript-downloader';

@Component({
  selector: 'app-backend-plant-picker',
  templateUrl: './backend-plant-picker.component.html',
  styleUrls: ['./backend-plant-picker.component.css']
})
export class BackendPlantPickerComponent implements OnInit, OnDestroy {

  plantList: Array<PlantDesc> = new Array<PlantDesc>();
  private timeout = false;


  constructor(
    private plantListService: PlantListService,
    private routerExtensions: RouterExtensions,
    private _changeDetectionRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.initDataItems();
    Downloader.init();
  }

  ngOnDestroy() {
    this.plantList = null;
  }

  private initDataItems() {
    this.plantListService.plantList.forEach(el => {
      const index = this.plantList.findIndex(p => p.id === el.id || p.name === el.name)
      if (index > -1) {
        this.plantList[index] = el
      } else {
        this.plantList.push(el)
      }
    })
    this._changeDetectionRef.detectChanges();
  }

  isEmpty() {
    return this.plantList.length === 0;
  }

  public onPullToRefreshInitiated(args: ListViewEventData) {
    this.plantListService.updatePlants().then(() => {
      this.initDataItems();
    }).then(() => {
      const listView = args.object;
      listView.notifyPullToRefreshFinished();
    }).catch(err => console.log(err));

  }

  async getFile(plant: PlantDesc) {
    return this.plantListService.getImage(plant.imageURL)
      .then(f => {
        if (f === null) {
          //console.log("no path")
          return ""
        }
        return f.path
      });
  }

  hasPlantObj(imageURL: string): boolean {
    return this.plantListService.hasPlant(imageURL);
  }

  async onCheckedChange(imageURL: string) {
    this.timeout = true;
    //console.log("image url: " + imageURL)
    if (this.hasPlantObj(imageURL) === false) {
      await this.plantListService.getPlantObject(imageURL)
    } else {
      this.plantListService.delPlantObject(imageURL)
    }
    this.initDataItems();
    setTimeout(() => {
      this.timeout = false
    }, 2000)
  }

  goBack(): void {
    this.routerExtensions.back();
  }
}
