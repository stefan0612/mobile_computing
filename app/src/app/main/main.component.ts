import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { LocalDbService } from "../service/local-db/local-db.service";
import * as geolocation from "nativescript-geolocation";
import { PlantListService } from "../service/plant-list/plant-list.service";
@Component({
    selector: "Main",
    templateUrl: "./main.component.html"
})
export class MainComponent implements OnInit {

    private isEnabled = false;

    constructor(
        private _changeDetectionRef: ChangeDetectorRef,
        private localDbService: LocalDbService) {
    }

    showDisplay() {
        return (this.isEnabled === true) ? 'inherit' : 'none'
    }


    ngAfterViewInit() {
        this._changeDetectionRef.detectChanges();
    }

    ngOnInit(): void {
        this.askGeoLocation()

    }

    askGeoLocation() {
        geolocation.isEnabled().then(isEnabled => {
            if (!isEnabled) {
                geolocation.enableLocationRequest(true, true).then(() => {
                    this.isEnabled = true;
                }, (e) => {
                    console.log((e.message || e));
                }).catch(ex => {
                    console.log("Unable to Enable Location", ex);
                });
            } else {
                this.isEnabled = true;
            }
        }, (e) => {
            console.log((e.message || e));
        });
    }
}
