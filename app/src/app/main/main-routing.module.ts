import { BackendPlantPickerComponent } from "./backend-plant-picker/backend-plant-picker.component";
import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { MainComponent } from "./main.component";
import { UserSessionControlComponent } from "./user-session-control/user-session-control.component";

const routes: Routes = [
    { path: "", component: MainComponent },
    { path: "picker", component: BackendPlantPickerComponent },
    { path: "session", component: UserSessionControlComponent },
    { path: "ar", loadChildren: () => import("~/app/ar-session/ar-session.module").then((m) => m.ArModule) }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class MainRoutingModule { }
