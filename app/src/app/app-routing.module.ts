import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
    { path: "", redirectTo: "/main", pathMatch: "full" },
    { path: "main", loadChildren: () => import("~/app/main/main.module").then((m) => m.MainModule) },
    { path: "ar", loadChildren: () => import("~/app/ar-session/ar-session.module").then((m) => m.ArModule) }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
