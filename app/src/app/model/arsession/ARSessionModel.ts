import { ARCommonNode } from "nativescript-ar"
import { SingleArModel } from "./SingleArModel"

export class ARSessionModel {

    private _modelName: string
    private _modelPosition: SingleArModel

    public get modelName(): string {
        return this._modelName
    }
    public set modelName(value: string) {
        this._modelName = value
    }

    public get modelPosition(): SingleArModel {
        return this._modelPosition
    }
    public set modelPosition(value: SingleArModel) {
        this._modelPosition = value
    }

    constructor(modelName: string, modelPosition: SingleArModel) {
        this._modelName = modelName
        this._modelPosition = modelPosition
    }
}