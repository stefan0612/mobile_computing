import { ARCommonNode } from "nativescript-ar"

export class LiveARSessionModel {
    private _modelName: string
    private _modelPosition: ARCommonNode
    private _scale: number
    private _rotate: number

    public get modelName(): string {
        return this._modelName
    }
    public set modelName(value: string) {
        this._modelName = value
    }

    public get modelPosition(): ARCommonNode {
        return this._modelPosition
    }
    public set modelPosition(value: ARCommonNode) {
        this._modelPosition = value
    }

    public get scale(): number {
        return this._scale
    }
    public set scale(value: number) {
        this._scale = value
    }

    public get rotate(): number {
        return this._rotate
    }
    public set rotate(value: number) {
        this._rotate = value
    }

    constructor(modelName: string, modelPosition: ARCommonNode, scale: number = 1, rotate: number = 0) {
        this._modelName = modelName
        this._modelPosition = modelPosition
        this._scale = scale
        this._rotate = rotate
    }
}