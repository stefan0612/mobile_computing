export class SingleArModel {
    private _x: number

    private _y: number

    private _z: number

    private _scale: number

    private _rotate: number

    public get x(): number {
        return this._x
    }
    public set x(value: number) {
        this._x = value
    }
    public get y(): number {
        return this._y
    }
    public set y(value: number) {
        this._y = value
    }
    public get z(): number {
        return this._z
    }
    public set z(value: number) {
        this._z = value
    }
    public get scale(): number {
        return this._scale
    }
    public set scale(value: number) {
        this._scale = value
    }
    public get rotate(): number {
        return this._rotate
    }
    public set rotate(value: number) {
        this._rotate = value
    }

    constructor(x: number, y: number, z: number, scale: number, rotate: number) {
        this._x = x
        this._y = y
        this._z = z
        this._scale = scale
        this._rotate = rotate
    }
}