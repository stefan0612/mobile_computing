export class ARListItem {
    name: string
    date: Date
    inCloud: boolean
    constructor(
        name: string,
        date: Date,
        inCloud: boolean
    ) {
        this.name = name
        this.date = date
        this.inCloud = inCloud
    }
}