import { ARSessionModel } from "./ARSessionModel"
import { Location } from "nativescript-geolocation";

export class ARModelPosition {
    id: string
    name: string
    date: Date
    models: Array<ARSessionModel>
    pos: Location;
    inCloud: boolean;

    constructor(name: string, date: Date, models: Array<ARSessionModel>, pos: Location, id: string = null, inCloud: boolean = false) {
        this.name = name
        this.date = date
        this.models = models
        this.pos = pos
        this.id = id
        this.inCloud = inCloud
    }
}