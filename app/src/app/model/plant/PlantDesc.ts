export class PlantDesc {
    private _id: string
    private _name: string
    private _properties: string[]
    private _imageURL: string
    private _localImg: string

    public get id(): string {
        return this._id
    }

    public get name(): string {
        return this._name
    }

    public get properties(): string[] {
        return this._properties
    }

    public get imageURL(): string {
        return this._imageURL
    }

    public get localImg(): string {
        return this._localImg
    }
    public set localImg(value: string) {
        this._localImg = value
    }

    constructor(id: string, name: string, properties: string[], imageURL: string, localImg?: string) {
        this._id = id
        this._name = name
        this._properties = properties
        this._imageURL = imageURL
        this._localImg = localImg
    }
}