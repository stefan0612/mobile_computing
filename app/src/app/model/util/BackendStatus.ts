export class BackendStatus {
    status: string
    info: string

    constructor(status: string, info: string) {
        this.status = status
        this.info = info
    }
}