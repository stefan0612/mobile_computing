import { environment } from "./../../../environments/environment";
import { Downloader, ProgressEventData, DownloadEventData } from 'nativescript-downloader';
import { Folder, File, path, knownFolders } from "tns-core-modules/file-system";
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlantModelService {

  appendURI = "content";
  fdownloader: Downloader = new Downloader();

  objFolder: Folder;



  constructor() {
    this.objFolder = knownFolders.documents().getFolder("plantObj");
    // this.fDownLoad.addProgressCallback((progress) => {
    //   console.log('Progress:', progress, 'URL:', 'url', 'Destination', 'destination');
    // });
  }

  hasPlantObject(name: string): boolean {
    const currentImg = path.join(this.objFolder.path, name + ".glb");
    if (File.exists(currentImg) === true) {
      //const f = File.fromPath(currentImg)
      //console.log("Found obj " + f.name + " in path " + f.path + " with size:" + f.size / 1000000 + " MB")
      return true;
    } else {
      //console.log("Found no obj ")
      return false;
    }
  }

  async getPlantObject(name: string): Promise<File | null> {
    const fName = name + ".glb"
    const uri = environment.serverUrl + this.appendURI + "/object/" + fName
    const currentImg = path.join(this.objFolder.path, fName);
    //console.log("Plant obs " + uri)
    if (File.exists(currentImg) === false) {
      const modelDownloander = this.fdownloader.createDownload({
        url: uri,
        path: this.objFolder.path,
        fileName: fName
      })
      const downloadProgress = this.fdownloader.start(modelDownloander, (progressData: ProgressEventData) => {
        console.log('fileProgress', progressData.value)
      })
      return downloadProgress
        .then(result => {
          if (result) {
            console.log("Downloaded: " + result.path + ", Status: " + result.status)
            return result;
          } else {
            console.log("No file found with url " + uri)
            return null;
          }
        })
        .catch(err => {
          console.log("Found Error in Plant-Model service", err)
          return err;
        })
    } else {
      console.log("file local")
      return new Promise((resolve, reject) => resolve(File.fromPath(currentImg)));
    }
  }

  delPlantObject(name: string) {
    const fileName = name + ".glb"
    const currentImg = path.join(this.objFolder.path, fileName);
    if (File.exists(currentImg) === true) {
      File.fromPath(currentImg).remove()
        .then(res => {
          console.log("File successfully deleted!")
        }).catch(err => {
          console.log(err);
        });
    }
  }
}
