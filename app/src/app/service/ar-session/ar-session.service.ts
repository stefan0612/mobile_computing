import { ARModelPosition } from "./../../model/arsession/ARModelPosition";
import { Injectable } from '@angular/core';
import { BackendRequestService } from '../backend-request/backend-request.service';
import { LocalDbService } from '../local-db/local-db.service';
import { ARListItem } from "~/app/model/arsession/ARListItem";

@Injectable({
  providedIn: 'root'
})
export class ArSessionService {

  private _currentSession: string | undefined = undefined;

  public get currentSession(): string | undefined {
    return this._currentSession;
  }

  constructor(private db: LocalDbService, private backend: BackendRequestService) {

  }

  setStartSession(name: string) {
    this._currentSession = name
  }

  async addARSession(session: ARModelPosition) {
    return this.db.addARSession(session)
    //.then(() => this.backend.updateUserSession(session));
  }

  async updateARSession(session: ARModelPosition) {
    return this.db.updateARSession(session);
  }

  async delARSession(name: string) {
    return this.db.delARSession(name)
      .then(() => this.backend.delUserSession(name))
      .catch(err => {
        console.log(err);
        return err;
      });
  }

  async getARSessionName(name?: string): Promise<ARListItem[]> {
    return this.db.getARSessionName(name)
  }

  async getARSession(name?: string): Promise<ARModelPosition[]> {
    return this.db.getARSession(name)
  }

  async syncBackend() {
    const loadFromCloud = this.backend.getUserSession()
      .then((backendSession) => {
        if (backendSession instanceof Array && backendSession.length === 0) {
          console.log("empty backend")
          return this.db.getARSession().then((dbSession) => this.updateBackend(dbSession))
        }
        else if (backendSession instanceof Array && backendSession.length > 0) {
          return backendSession;
        } else {
          throw new Error("None connection to backend: " + backendSession);
        }
      }, err => {
        console.log(err);
        return err;
      });
    // Update Local Changes to the cloud
    const updateCloud = loadFromCloud.then(
      (backendSession) => {
        return this.db.getARSession()
          .then((dbSession) => {
            const fromLocal = this.filterNotContained(dbSession, backendSession);
            console.log(fromLocal)
            return this.updateBackend(fromLocal);
          });
      });
    // Update Cloud Changes to local
    const updateLocal = updateCloud
      .then(() => this.backend.getUserSession())
      .then(
        (backendSession: ARModelPosition[]) => {
          return this.db.getARSession()
            .then(
              (dbSession) => {
                const fromBackend = this.filterNotContained(backendSession, dbSession);
                return Promise.all(fromBackend.map(i => this.updateARSession(i)));
              });
        });
    return updateLocal.then(
      (result) => {
        console.log("update local")
        return this.updateLocalSync();
      });
  }

  private async updateLocalSync() {
    return this.backend.getUserSession()
      .then(async (backendSession) => {
        if (backendSession instanceof Array && backendSession.length > 0) {
          const dbSession = await this.db.getARSession();
          const inCommon = this.filterContained(backendSession, dbSession);
          //console.log(inCommon)
          if (inCommon.length > 0) {
            return inCommon.map(eachSession => this.updateARSession(eachSession));
          }
        } else {
          return null;
        }
      })
  }

  private cmpSession(first: ARModelPosition, second: ARModelPosition): boolean {
    // console.log(first)
    // console.log(second)
    return first.name === second.name && first.models.toString() === second.models.toString() && first.date.toUTCString() === second.date.toUTCString();
  }

  // Return all Elements from first withoud Elements from second
  // Complement from first and second.
  // First / Second
  private filterNotContained(first: ARModelPosition[], second: ARModelPosition[]): ARModelPosition[] {
    return first.filter(fSession => second.findIndex(sSession => this.cmpSession(sSession, fSession)) < 0);
  }

  private filterContained(first: ARModelPosition[], second: ARModelPosition[]): ARModelPosition[] {
    const filteredList = first.filter(fSession => second.findIndex(sSession => this.cmpSession(sSession, fSession)) !== -1)
    if (filteredList && filteredList.length > 0) {
      //console.log(filteredList.length)
      return filteredList.map(allSession => {
        allSession.inCloud = true
        return allSession;
      });
    } else {
      return [];
    }

  }

  private async updateBackend(session: ARModelPosition[]) {
    //console.log(session)
    return this.backend.updateUserSession(session);
  }
}
