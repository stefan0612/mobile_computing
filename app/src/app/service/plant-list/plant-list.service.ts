import { PlantModelService } from "./../plant-model/plant-model.service";
import { PlantDesc } from './../../model/plant/PlantDesc';
import { Injectable } from '@angular/core';
import { LocalDbService } from '../local-db/local-db.service';
import { BackendRequestService } from '../backend-request/backend-request.service';
import { BackendStatus } from '~/app/model/util/BackendStatus';
import { File } from "tns-core-modules/file-system";

@Injectable({
  providedIn: 'root',
})
export class PlantListService {

  plantList: Array<PlantDesc> = new Array<PlantDesc>();

  private selectedPlant: PlantDesc = null

  constructor(private db: LocalDbService, private http: BackendRequestService, private plantModel: PlantModelService) {
    this.receivePlantDescFromDB()
    this.updatePlants()
  }

  getPlants(filter?: string) {
    return (filter && filter.length > 0) ? this.plantList.filter(p => this.complexFilter(filter, p)) : this.plantList
  }

  private complexFilter(filterName: string, obj: PlantDesc): boolean {
    return (
      filterName.toLocaleLowerCase() === obj.name.toLocaleLowerCase() ||
      obj
        .properties
        .filter(
          p => p.toLocaleLowerCase() === filterName.toLocaleLowerCase()
        ).length > 0
    )
  }

  isSelected(): PlantDesc | boolean {
    return this.selectedPlant || false
  }

  unselect() {
    this.selectedPlant = null
  }

  receivePlantDescFromDB() {
    this.db.getPlantDesc().then(row => {
      row.forEach(r => this.plantList.push(r))
    })
  }

  async updatePlants() {
    return this.db.getPlantDesc().then((plant) => {
      this.updatePlantDesc(plant);
    }).then(() => {
      this.http.getPlantDesc(null).then((res) => {
        const responseList: PlantDesc[] | BackendStatus = res
        if (responseList instanceof BackendStatus) {
          console.log("Error:" + responseList.status + " because:" + responseList.info);
        } else {
          this.updatePlantDesc(responseList)
        }
      })
    })
    // .then(() => {
    //   this.plantList.map((p) => {
    //     this.db.addPlantDesc(p)
    //   })
    // });
  }

  private updatePlantDesc(elem: PlantDesc[]) {
    if (elem && elem.length > 0)
      elem.forEach(e => {
        //console.log("found object: " + e.name)
        this.getImage(e.imageURL).then((file) => {
          const newObj = new PlantDesc(e.id, e.name, e.properties, e.imageURL, file.path)
          const i = this.plantList.findIndex(plant => plant.name === e.name)
          if (i > -1) {
            this.plantList[i] = newObj;
            this.db.delPlantDesc(newObj.name)
          } else {
            this.plantList.push(newObj);
          }
          this.db.addPlantDesc(newObj)
        })
      })
  }

  async getImage(plantImgName: string): Promise<File> {
    return this.http.getPlantImg(plantImgName)
    // .then(f => {
    //   if (f) {
    //     console.log("Found file " + f.name + " in path " + f.path + " with size:" + f.size / 1000000 + " MB")
    //     return f;
    //   }
    //   return f;
    // })
  }

  delPlantObject(imageURL: string) {
    this.plantModel.delPlantObject(imageURL);
  }

  hasPlant(imageURL: string): boolean {
    return this.plantModel.hasPlantObject(imageURL)
  }

  async getPlantObject(imageURL: string): Promise<File> {
    //console.log("get Plant Image with name: " + imageURL)
    return this.plantModel.getPlantObject(imageURL)
      .then(f => {
        if (f) {
          //console.log("file loaded")
          return f;
        }
        //console.log("no file loaded")
        return null;
      }).catch(err => {
        console.log("Plant List Service:")
        console.log(err)
        return err;
      })
  }
}
