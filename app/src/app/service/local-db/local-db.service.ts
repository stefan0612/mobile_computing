import { ARSessionModel } from "./../../model/arsession/ARSessionModel";
import { Injectable } from '@angular/core';
import { NativeSQLite } from "@nano-sql/adapter-sqlite-nativescript";
import { nSQL } from "@nano-sql/core/lib/index";
import { InanoSQLTableConfig } from '@nano-sql/core/lib/interfaces';
import { setErrorHandler } from 'tns-core-modules/trace';
import { ARModelPosition } from '~/app/model/arsession/ARModelPosition';
import { PlantDesc } from '~/app/model/plant/PlantDesc';
import { Location } from "nativescript-geolocation";
import { SingleArModel } from "~/app/model/arsession/SingleArModel";
import * as geolocation from "nativescript-geolocation";
import { colorProperty } from "tns-core-modules/ui/page";
import { ARListItem } from "~/app/model/arsession/ARListItem";
@Injectable({
  providedIn: 'root'
})
export class LocalDbService {

  tables: InanoSQLTableConfig[] = [
    {
      name: "PlantDesc",
      model: {
        "id:uuid": { pk: true },
        "name:string": { notNull: true },
        "properties:string[]": { default: [] },
        "imageURL:string": { notNull: true },
        "localIMG: string": {},
      }
    },
    {
      name: "ARSession",
      model: {
        "id:uuid": { pk: true, ai: true },
        "name:string": { notNull: true },
        "date:date": { notNull: true },
        "pos:any": { notNull: true },
        "models:any[]": { default: [] },
        "inCloud:boolean": { default: false },
      }
    }
  ];

  constructor() {
    this.connectDB()
  }

  async connectDB() {
    if (nSQL().listDatabases().length == 0) {
      nSQL().createDatabase({
        id: "local-db",
        mode: new NativeSQLite(),
        tables: this.tables
      })
    }
    nSQL().useDatabase("local-db")
  }

  cleanDB() {
    return nSQL("PlantDesc").query("delete")
  }

  addPlantDesc(plant: PlantDesc) {
    return nSQL("PlantDesc").loadJS([plant])
  }

  delPlantDesc(name: string) {
    return nSQL("PlantDesc").query("delete").where(["name", "=", name]).exec()
  }

  async getPlantDesc(): Promise<PlantDesc[]> {
    return nSQL("PlantDesc")
      .query("select")
      .exec()
      .then(row => {
        if (row.length > 0) {
          return row.map(r => r as PlantDesc)
            .map(r => {
              return r;
            })
        } else {
          return []
        }
      })
  }

  addARSession(session: ARModelPosition) {
    return nSQL("ARSession").loadJS(
      [
        {
          id: session.id,
          name: session.name,
          date: session.date,
          pos: session.pos,
          models: session.models,
          inCloud: session.inCloud,
        }
      ])
  }

  async updateARSession(session: ARModelPosition) {
    await this.delARSession(session.name);
    this.addARSession(session);
  }

  delARSession(name: string) {
    return nSQL("ARSession").query("delete").where(["name", "=", name]).exec()
  }

  async getARSession(name?: string): Promise<ARModelPosition[]> {
    const arSessions = nSQL("ARSession").query("select")
    const arSessionFilter = name ? arSessions.where(["name", "=", name]) : arSessions;
    return arSessionFilter
      .exec()
      .then(row => {
        if (row.length > 0) {
          //console.log(row)
          return row.map(r => {
            const id = r['id']
            const name = r['name']
            const date = new Date(r['date'])
            const models = r['models'] as string[]
            const modelsToObj: ARSessionModel[] = models.map(e => {
              const modelPosPlane = e['_modelPosition']
              const x = JSON.parse(modelPosPlane['_x']) as number
              const y = JSON.parse(modelPosPlane['_y']) as number
              const z = JSON.parse(modelPosPlane['_z']) as number
              const scale = JSON.parse(modelPosPlane['_scale']) as number
              const rotate = JSON.parse(modelPosPlane['_rotate']) as number
              const modelPos = new SingleArModel(x, y, z, scale, rotate)
              return new ARSessionModel(e['_modelName'], modelPos)
            })
            const posObj = r['pos']
            let pos = new geolocation.Location()
            pos.latitude = posObj['latitude']
            pos.longitude = posObj['longitude']
            pos.altitude = posObj['altitude']
            pos.horizontalAccuracy = posObj['horizontalAccuracy']
            pos.verticalAccuracy = posObj['verticalAccuracy']
            pos.speed = posObj['speed']
            pos.direction = posObj['direction']
            pos.timestamp = new Date()
            const inCloud = r['inCloud'] as boolean;
            return new ARModelPosition(name, date, modelsToObj, pos, id, inCloud)
          })
            .map(r => {
              return r;
            })
        } else {
          return []
        }
      })
  }

  async getARSessionName(name?: string): Promise<ARListItem[]> {
    const arSessions = nSQL("ARSession").query("select", ["name", "date", "inCloud"])
    const arSessionFilter = name ? arSessions.where(["name", "=", name]) : arSessions;
    return arSessionFilter
      .exec()
      .then(row => {
        if (row.length > 0) {
          //console.log(row)
          return row.map(r => new ARListItem(r["name"], r["date"] as Date, r["inCloud"] as boolean))
            .map(r => {
              return r;
            })
        } else {
          return []
        }
      })
  }

  // export() {
  //   nSQL().rawDump([], false, (table: string, row) => {
  //     console.log(table + " : " + row)
  //   })
  // }
}


