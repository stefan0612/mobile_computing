import { environment } from "./../../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Folder, File, path, knownFolders } from "tns-core-modules/file-system";
import { ImageSource } from "tns-core-modules/image-source";
import { Injectable } from '@angular/core';
import { PlantDesc } from '~/app/model/plant/PlantDesc';
import { BackendStatus } from '~/app/model/util/BackendStatus';
import { ARModelPosition } from "~/app/model/arsession/ARModelPosition";

@Injectable(
  {
    providedIn: 'root'
  }
)
export class BackendRequestService {

  appendURI = "content";
  userSessionURI = "user/session";

  imgFolder: Folder;

  headers = new HttpHeaders({
    "Content-Type": "application/json",
  });

  constructor(private http: HttpClient) {
    this.imgFolder = knownFolders.documents().getFolder("plantImg");
  }

  async getPlantDesc(name: string): Promise<PlantDesc[] | BackendStatus> {
    const uri = environment.serverUrl + this.appendURI + "/desc" + (name ? `/${name}` : "")
    //console.log(uri)
    return this.http.get<PlantDesc[] | BackendStatus>(uri, { headers: this.headers }).toPromise()
  }

  async getPlantImg(name: string): Promise<File | null> {
    const fileNameType = name + ".png";
    if (name === null) {
      return new Promise((resolve, reject) => reject("Plant Image Name not set"));
    }
    const uri = environment.serverUrl + this.appendURI + "/img/" + fileNameType
    const currentImg = path.join(this.imgFolder.path, fileNameType);
    if (File.exists(currentImg) === false) {
      //console.log("file isnt local")
      return ImageSource.fromUrl(uri).then((img) => {
        return img.saveToFile(currentImg, "png");
      }).then((isWritten) => {
        if (isWritten === true) {
          console.log("file downloaded")
          return File.fromPath(currentImg);
        } else {
          //console.log("file isnt downloaded")
          return null;
        }
      });
    } else {
      //console.log("file local")
      return new Promise((resolve, reject) => resolve(File.fromPath(currentImg)));
    }
  }

  async getUserSession(name?: string): Promise<ARModelPosition[] | BackendStatus> {
    const uri = environment.serverUrl + this.userSessionURI + (name ? `/${name}` : "")
    //console.log(uri)
    return this.http.get<ARModelPosition[] | BackendStatus>(uri, { headers: this.headers }).toPromise()
      .then((resp) => {
        if (resp instanceof Array && resp.length > 0) {
          return resp.map(i => {
            i.date = new Date(i.date.toString());
            return i;
          })
        }
        return [];
      })
  }

  async updateUserSession(userSession: ARModelPosition[] | ARModelPosition): Promise<BackendStatus | any> {
    const uri = environment.serverUrl + this.userSessionURI;
    //console.log(userSession)
    return this.http.post<BackendStatus | any>(uri, userSession, { headers: this.headers }).toPromise()
  }

  async delUserSession(name: string): Promise<any> {
    const uri = environment.serverUrl + this.userSessionURI + "/" + name;
    //console.log(uri)
    return this.http.delete(uri, { headers: this.headers }).toPromise();
  }


}
