import { PlantPickerComponent } from './plant-picker/plant-picker.component';
import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { ArSessionComponent } from "./ar-session.component"

const routes: Routes = [
    { path: "", component: ArSessionComponent, 
        children: [
            {
                path: "plant-modal", component: PlantPickerComponent
            }
        ] },
   { path: "main", loadChildren: () => import("~/app/main/main.module").then((m) => m.MainModule) },
   { path: "app", loadChildren: "~/app/app.module#AppModule" }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ArSessionRoutingModule { }
