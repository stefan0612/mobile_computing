import { ARModelPosition } from "~/app/model/arsession/ARModelPosition";
import { ChangeDetectorRef, Component, OnInit, ViewContainerRef } from '@angular/core';
import { Color } from "tns-core-modules/color";
import { AR, ARMaterial, ARNodeInteraction, ARPlaneTappedEventData, ARLoadedEventData, ARCommonNode, ARPlaneDetectedEventData, ARPosition, ARScale, ARRotation } from "nativescript-ar";
import { ModalDialogOptions, ModalDialogService } from "nativescript-angular/modal-dialog";
import { PlantListService } from '../service/plant-list/plant-list.service';
import { ModalRootComponent } from './modal-root/modal-root.component';
import { PlantDesc } from '../model/plant/PlantDesc';
import { RouterExtensions } from 'nativescript-angular/router';
import { ActivatedRoute } from '@angular/router';
import { registerElement } from "nativescript-angular/element-registry";
import { action, prompt, capitalizationType, confirm, inputType, PromptOptions } from "tns-core-modules/ui/dialogs";
import { ArSessionService } from '../service/ar-session/ar-session.service';
import { ARSessionModel } from '../model/arsession/ARSessionModel';
import * as geolocation from "nativescript-geolocation";
import { Accuracy } from "tns-core-modules/ui/enums";
import { LiveARSessionModel } from "../model/arsession/LiveARSessionModel";
import { SingleArModel } from "../model/arsession/SingleArModel";
import { Page } from "tns-core-modules/ui/page";
import { Slider } from "tns-core-modules/ui/slider";
import { titleCase } from "@nano-sql/core/lib/utilities";
import { ThrowStmt } from "@angular/compiler";
registerElement("AR", () => require("nativescript-ar").AR);

@Component({
    selector: 'app-ar-session',
    providers: [ModalDialogService],
    templateUrl: './ar-session.component.html',
    styleUrls: ['./ar-session.component.css']
})
export class ArSessionComponent implements OnInit {


    private worldTracking: AR

    private placedModels: Array<LiveARSessionModel> = new Array()

    private selectedModel: PlantDesc

    private selectedPlant: ARCommonNode = null;
    private selectedPlantImg: string = null;
    private modifyMode: boolean = false;
    private delMode: boolean = false

    private addButtonSymbol = String.fromCharCode(0xf067)
    private delModeButtonSymbol = String.fromCharCode(0xf2ed)
    private modModeButtonSymbol = String.fromCodePoint(0xf013)

    private addButtonColor = "mediumblue";
    private delModeButtonColor = "gray";
    private modModeButtonColor = "orange";

    private showModBars = false;

    private placePlantSession = false;


    constructor(
        private plantService: PlantListService,
        private arSession: ArSessionService,
        private _modalService: ModalDialogService,
        private _vcRef: ViewContainerRef,
        private routerExtensions: RouterExtensions,
        private activeRoute: ActivatedRoute,
        private page: Page,
        private _changeDetectionRef: ChangeDetectorRef
    ) {
        page.actionBarHidden = true;
    }

    arLoaded(args: ARLoadedEventData): void {
        console.log("Loaded")
        this.worldTracking = args.object;
    }

    planeDetected(args: ARPlaneDetectedEventData) {
        console.log("new plane deteted")
    }

    ngOnInit() {
        const choosed = this.plantService.isSelected();
        if (typeof choosed === "string") {
            this.plantService.unselect()
        }
        if (typeof this.arSession.currentSession === "string") {
            this.placePlantSession = true;
            // setTimeout(() => {
            //     this.loadSavedSession()
            // }, 5000);
        }
    }

    placePlantsFromSession() {
        if (this.placePlantSession === true) {
            this.loadSavedSession().then(() => {
                this.placePlantSession = false;
            })
        }
    }

    movePlant(args: ARPosition) {
        if (this.selectedPlant !== null) {
            this.selectedPlant.setWorldPosition(args)
        }
    }

    addNewModel(arObj: AR, model: ARSessionModel | PlantDesc, args?: ARPosition): Promise<any> {
        return this.getPlantObj(model).then(filepath => {
            const position = (model instanceof PlantDesc) ? { x: args.x, y: args.y, z: args.z } : model.modelPosition
            const scale = (model instanceof ARSessionModel) ? model.modelPosition.scale : { x: 1, y: 1, z: 1 } as ARScale;
            const rotate: ARRotation = (model instanceof ARSessionModel) ? { x: 0, y: model.modelPosition.rotate, z: 0 } as ARRotation : { x: 0, y: 0, z: 0 } as ARRotation;
            const modelName = (model instanceof PlantDesc) ? model.imageURL : model.modelName;
            const cName = `${modelName}  ${position}`
            const newNode = arObj.addModel({
                name: filepath,
                scale: scale,
                rotation: rotate,
                mass: 20,
                position: position,
                scalingEnabled: false,
                draggingEnabled: false,
                rotatingEnabled: false,
                childNodeName: cName,
                onTap: ((interaction: ARNodeInteraction) => {
                    console.log("tap")
                    if (this.delMode === true) {
                        console.log("removed")
                        this.placedModels = this.placedModels.filter(m => m.modelPosition.id !== interaction.node.id);
                        interaction.node.remove();
                    }
                    if (this.modifyMode === true) {
                        if (this.selectedPlant === interaction.node) {
                            console.log("node unset")
                            this.selectedPlantImg = null;
                            this.selectedPlant = null;
                            this.showModBars = false;
                            this._changeDetectionRef.detectChanges()
                        } else {
                            console.log("node id set")
                            this.selectedPlant = interaction.node;
                            const selectedModelName = this.placedModels.filter(m => m.modelPosition.id === interaction.node.id)[0].modelName;
                            this.plantService.getImage(selectedModelName).then(value => {
                                this.selectedPlantImg = value.path
                                this._changeDetectionRef.detectChanges()
                            })
                        }
                    }
                })
            });
            return newNode.then((addedModel) => {
                const modelName = (model instanceof PlantDesc) ? model.imageURL : model.modelName;
                const modelScale = (model instanceof PlantDesc) ? 1 : model.modelPosition.scale;
                const modelRotate = (model instanceof PlantDesc) ? 1 : model.modelPosition.rotate;
                //console.log(addedModel)
                return this.placedModels.push(new LiveARSessionModel(modelName, addedModel, modelScale, modelRotate));
            })
                .catch(err => {
                    console.log("AddModel in Session Error")
                    console.log(err)
                    return err
                });
        })
    }

    async loadSavedSession() {
        if (this.worldTracking && this.worldTracking.isLoaded) {
            this.arSession.getARSession(this.arSession.currentSession)
                .then((startSession) => {
                    if (startSession.length < 1) {
                        console.log("no start session found")
                        return
                    }
                    const startS = startSession[0];
                    startS.models.forEach(async (model: ARSessionModel) => {
                        this.addNewModel(this.worldTracking, model)
                    })

                }).then(() => {
                    this.arSession.setStartSession(undefined)
                })
        } else {
            console.log("timeout")
            setTimeout(() => this.loadSavedSession(), 5000)
        }
    }

    planeMaterial = <ARMaterial>{
        diffuse: new Color("white"),
        ransparency: 0.2
    };

    async getPlantObj(plant: PlantDesc | ARSessionModel) {
        if (plant instanceof ARSessionModel) {
            return this.plantService.getPlantObject(plant.modelName).then(f => f.path);
        } else {
            return this.plantService.getPlantObject(plant.imageURL).then(f => f.path);
        }
    }

    planeTapped(args: ARPlaneTappedEventData) {
        const ar: AR = args.object;
        if (this.selectedModel && this.delMode === false && this.modifyMode === false) {
            this.addNewModel(ar, this.selectedModel, Object.assign({}, args.position))
        }
        if (this.delMode === false && this.modifyMode === true) {
            this.movePlant(args.position);
        }
        this._changeDetectionRef.detectChanges()
        // else {
        //     ar.addBox({
        //         position: {
        //             x: args.position.x,
        //             y: args.position.y,
        //             z: args.position.z
        //         },
        //         dimensions: {
        //             x: 0.1,
        //             y: 0.1,
        //             z: 0.1
        //         },
        //         draggingEnabled: true,
        //         scalingEnabled: true,
        //         mass: 20,
        //         materials: [new Color("blue")],
        //         // onLongPress: ((interaction: ARNodeInteraction) => {
        //         //     interaction.node.remove();
        //         //     this.placedModels = this.placedModels.filter(model => model)
        //         // })
        //     })//.then(model => this.placedModels.push(new LiveARSessionModel("ey", model)))
        // }
    }

    changeMode(input: string) {
        switch (input) {
            case 'del':
                this.delMode = !this.delMode;
                this.modifyMode = false;
                this.selectedPlant = null;
                this.selectedPlantImg = null;
                this.showModBars = false;
                break;
            case 'mod':
                this.modifyMode = !this.modifyMode;
                this.delMode = false;
                break;
            default:
                break;
        }
        if (this.delMode === true) {
            this.addButtonSymbol = String.fromCharCode(0xf2ed)
            this.delModeButtonSymbol = String.fromCharCode(0xf067)
            this.modModeButtonSymbol = String.fromCodePoint(0xf013)

            this.addButtonColor = "gray"
            this.delModeButtonColor = "mediumblue"
            this.modModeButtonColor = "orange";
        } else if (this.modifyMode === true) {
            this.addButtonSymbol = String.fromCodePoint(0xf013)
            this.delModeButtonSymbol = String.fromCharCode(0xf2ed)
            this.modModeButtonSymbol = String.fromCharCode(0xf067)

            this.addButtonColor = "orange";
            this.delModeButtonColor = "gray"
            this.modModeButtonColor = "mediumblue"
        } else {
            this.addButtonSymbol = String.fromCharCode(0xf067)
            this.delModeButtonSymbol = String.fromCharCode(0xf2ed)
            this.modModeButtonSymbol = String.fromCharCode(0xf013)

            this.addButtonColor = "mediumblue"
            this.delModeButtonColor = "gray"
            this.modModeButtonColor = "orange";
            this.selectedPlant = null;
            this.selectedPlantImg = null;
            this.showModBars = false;
        }
    }

    rotateLeft() {
        if (this.selectedPlant !== null) {
            this.selectedPlant.rotateBy({
                x: 0,
                y: -10,
                z: 0,
            })
            const index = this.placedModels.findIndex(el => el.modelPosition.id === this.selectedPlant.id)
            if (index > -1) {
                this.placedModels[index].rotate -= 10
            }
        }
    }
    rotateRight() {
        if (this.selectedPlant !== null) {
            this.selectedPlant.rotateBy({
                x: 0,
                y: 10,
                z: 0,
            })
            const index = this.placedModels.findIndex(el => el.modelPosition.id === this.selectedPlant.id)
            if (index > -1) {
                this.placedModels[index].rotate += 10
            }
        }
    }

    scaleUp() {
        if (this.selectedPlant !== null) {
            this.selectedPlant.scaleBy(0.1)
            const index = this.placedModels.findIndex(el => el.modelPosition.id === this.selectedPlant.id)
            if (index > -1) {
                this.placedModels[index].scale += 0.1
            }
        }
    }
    scaleDown() {
        if (this.selectedPlant !== null) {
            this.selectedPlant.scaleBy(-0.1)
            const index = this.placedModels.findIndex(el => el.modelPosition.id === this.selectedPlant.id)
            if (index > -1) {
                this.placedModels[index].scale -= 0.1
            }
        }
    }

    addPlant() {
        if (this.delMode === true) {
            const lastItem = this.placedModels.pop();
            if (lastItem) {
                lastItem.modelPosition.remove();
            }
        }
        if (this.modifyMode === true) {
            this.showModBars = this.selectedPlant !== null ? !this.showModBars : false;
        } else {
            this.showModBars = false;
        }
        this._changeDetectionRef.detectChanges()
        if (this.modifyMode === false && this.delMode === false) {
            const options: ModalDialogOptions = {
                viewContainerRef: this._vcRef,
                context: {},
                fullscreen: true
            };

            this._modalService.showModal(ModalRootComponent, options)
                .then((result: PlantDesc) => {
                    if (result) {
                        this.selectedModel = result
                    }
                })
        }
    }

    clean() {
        let options = {
            title: "Clean Session",
            message: "Delete all placed plants?",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        confirm(options).then((res: boolean) => {
            if (res) {
                this.placedModels.forEach(model => model.modelPosition.remove())
                this.placedModels = new Array();
            }
        })

    }

    private transformToSave(elements: Array<LiveARSessionModel>): Array<ARSessionModel> {
        return elements.map(e => {
            const pos = e.modelPosition
            const singleModel = new SingleArModel(pos.getPosition().x, pos.getPosition().y, pos.getPosition().z, e.scale, e.rotate);
            //console.log(JSON.stringify(singleModel))
            return new ARSessionModel(e.modelName, singleModel)
        })
    }

    private newSave() {
        let options: PromptOptions = {
            title: "Save new Session",
            message: "Enter name for new session.",
            cancelButtonText: "No",
            okButtonText: "OK",
            cancelable: true,
            inputType: inputType.text,
            capitalizationType: capitalizationType.sentences
        };
        prompt(options).then((res) => {
            if (res.result && res.text.length > 0) {
                geolocation.getCurrentLocation({
                    desiredAccuracy: Accuracy.high,
                    maximumAge: 5000,
                    timeout: 10000,
                }).then((loc) => {
                    const saveSession = new ARModelPosition(res.text, new Date(), this.transformToSave(this.placedModels), loc)
                    this.arSession.addARSession(saveSession).then(value => {
                        console.log("save new session" + value)
                    })
                        .catch(err => {
                            console.log("Save New Session Error:")
                            console.log(err);
                        })
                });
            } else {
                console.log("cancled or wrong name")
            }
        })

    }

    private overrride() {
        this.arSession.getARSessionName().then(sessions => sessions.map(s => s['name'] + " at " + s['date'].toString())).then(s => {
            let options = {
                title: "Save Session",
                defaultText: "All saved sessions",
                message: "Save all placed plants?",
                cancelButtonText: "No",
                actions: s
            };
            action(options).then((res) => {
                if (res !== "No") {
                    this.arSession.getARSession()
                        .then(sessions => sessions.filter(s => s.name === res.split(' ')[0]))
                        .then(sessions => {
                            if (sessions.length > 0) {
                                var updateSessions = sessions[0];
                                updateSessions.date = new Date()
                                updateSessions.inCloud = false;
                                updateSessions.models = this.transformToSave(this.placedModels)
                                return updateSessions
                            } else {
                                console.log("No session to override")
                                return null;
                            }
                        }).then((updateSession) => {
                            this.overrideAccept(res).then(accepted => {
                                if (accepted === true) {
                                    console.log("Override accepted, save start")
                                    this.arSession.updateARSession(updateSession)
                                    console.log("Override accepted, save finished")
                                }
                            })
                        })
                }
            })
        })

    }

    overrideAccept(sessionName: string) {
        let options = {
            title: "Override Session",
            message: `These changes cannot be restored. Do you really want to override ${sessionName}?`,
            cancelButtonText: "No",
            okButtonText: "Yes",
        };
        return confirm(options)
    }

    save() {
        let options = {
            title: "Save Session",
            message: "Save all placed plants?",
            cancelButtonText: "No",
            actions: ["New", "Override"]
        };
        action(options).then((res: string) => {
            switch (res) {
                case "New": {
                    this.newSave();
                    break;
                }
                case "Override": {
                    this.overrride();
                    break;
                }
            }
        })
    }

    goBack(): void {
        let options = {
            title: "Exit Session",
            message: "You will lose the progress of everything not saved. Are you sure to leave?",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        confirm(options).then((res: boolean) => {
            if (res) {
                if (this.worldTracking && this.worldTracking.isLoaded) {
                    this.routerExtensions.back();
                }
            }
        })

    }

}
