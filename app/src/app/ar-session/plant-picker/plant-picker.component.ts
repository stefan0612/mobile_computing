import { ChangeDetectorRef, Component, OnInit, AfterViewInit } from "@angular/core";
import { RouterExtensions } from 'nativescript-angular/router';
import { SearchBar } from "tns-core-modules/ui/search-bar";
import { PlantDesc } from '~/app/model/plant/PlantDesc';
import { PlantListService } from '~/app/service/plant-list/plant-list.service';
import { ActivatedRoute } from "@angular/router";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { Page } from "tns-core-modules/ui/page";
import { ObservableArray } from 'tns-core-modules/data/observable-array';

@Component({
  selector: 'app-plant-picker',
  templateUrl: './plant-picker.component.html',
  styleUrls: ['./plant-picker.component.css']
})
export class PlantPickerComponent implements OnInit, AfterViewInit {
  searchPhrase: string;

  private showPlants: Array<PlantDesc>;

  constructor(
    private plantService: PlantListService,
    private _params: ModalDialogParams,
    private _page: Page,
    private router: RouterExtensions,
    private _activeRoute: ActivatedRoute,
    private _changeDetectionRef: ChangeDetectorRef
  ) {

  }

  private initDataItems(plants: PlantDesc[]) {
    const filterPlants = plants ? plants : [];
    this.showPlants = filterPlants.filter(el => this.hasPlantObj(el.imageURL))
  }

  ngOnInit() {
    this._page.actionBarHidden = true
  }

  ngAfterViewInit() {
    // this.initDataItems(this.plantService.getPlants());
    // this._changeDetectionRef.detectChanges();
  }

  onSubmit(args) {
    const searchBar = args.object as SearchBar;
    this.initDataItems(this.plantService.getPlants(searchBar.text))
    this._changeDetectionRef.detectChanges();
  }

  onTextChanged(args) {
    const searchBar = args.object as SearchBar;
  }

  onClear(args) {
    const searchBar = args.object as SearchBar;
    this.initDataItems(this.plantService.getPlants());
  }

  hasPlantObj(imageURL: string): boolean {
    return this.plantService.hasPlant(imageURL);
  }

  async getFile(imageURL: string) {
    return this.plantService.getImage(imageURL).then(f => f.path);
  }

  onTap(event) {
  }

  onClose(choosed?: PlantDesc): void {
    this._params.closeCallback(choosed);
  }
}
