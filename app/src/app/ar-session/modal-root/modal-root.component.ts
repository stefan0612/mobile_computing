import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'app-modal-root',
  templateUrl: './modal-root.component.html',
  styleUrls: ['./modal-root.component.css']
})
export class ModalRootComponent implements OnInit {
  constructor(
    private _routerExtensions: RouterExtensions,
    private _activeRoute: ActivatedRoute) {}

  ngOnInit(): void {
      this._routerExtensions.navigate(["plant-modal"], { relativeTo: this._activeRoute });
  }

}
