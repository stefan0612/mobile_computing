import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { CommonModule } from "~/app/common/common.module";
import { PlantListService } from "../service/plant-list/plant-list.service";
import { ArSessionRoutingModule } from "./ar-session-routing.module";
import { ArSessionComponent } from "./ar-session.component";
import { PlantPickerComponent } from './plant-picker/plant-picker.component';
import { ModalRootComponent } from './modal-root/modal-root.component';


@NgModule({
    imports: [
        NativeScriptUIListViewModule,
        NativeScriptCommonModule,
        ArSessionRoutingModule,
        CommonModule
    ],
    declarations: [
        ArSessionComponent,
        PlantPickerComponent,
        ModalRootComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    providers: [
        PlantListService
    ],
    entryComponents: [
        ModalRootComponent
    ]
})
export class ArModule { }
