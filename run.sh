#!/bin/bash
DEFAULTVAL="y"

echo "Start backend"
docker-compose up -d

cd app
tns doctor && tns device android --available-devices

read -p "Run app in emulator service [Y|n]: " -n 1 -s -r selected
selected=${selected:-${DEFAULTVAL}}
echo 
if [[ "$selected" =~ ^[yY]$ ]]
then
    echo "deploy app on emulated device"
    tns deploy android --env.environment=prod
else 
    echo "deploy app on real device"
    tns deploy android --env.environment=dev
fi
echo "Start App on device"
adb shell am start -n org.nativescript.plantar/com.tns.NativeScriptActivity
cd ../

read -p "Stop Backend [Y|n]: " -n 1 -s -r selected
selected=${selected:-${DEFAULTVAL}}
echo 
if [[ "$selected" =~ ^[yY]$ ]]; then
    echo "stop backend"
    docker-compose stop
fi