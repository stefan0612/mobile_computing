#!/bin/bash

URL='http://localhost:8080/content/'

URID="${URL}desc"
echo
echo "Upload plants"
curl -X POST -H "Content-Type: application/json" -d '{"name": "Flower", "properties": "green,small,single", "imageURL": "plant_2"}' $URID
curl -X POST -H "Content-Type: application/json" -d '{"name": "Tree", "properties": "green,big,tree", "imageURL": "plant_3"}' $URID
curl -X POST -H "Content-Type: application/json" -d '{"name": "Cactus", "properties": "green,small,pot,indoor", "imageURL": "plant_4"}' $URID
curl -X POST -H "Content-Type: application/json" -d '{"name": "Plant Pot", "properties": "green,small,pot,indoor", "imageURL": "plant_5"}' $URID

for img in ./preview/*
do
    FILENAME="$(basename "$img")"
    URI="${URL}img/${FILENAME}"
    echo
    echo "Upload file: ${FILENAME}"
    curl -X POST --data-binary "@$img" $URI
    sleep 2
done

for f in ./models/*
do
    FILENAME="$(basename "$f")"
    URI="${URL}object/${FILENAME}"
    echo
    echo "Upload file: ${FILENAME}"
    curl -X POST --data-binary "@$f" $URI
    sleep 2
done
