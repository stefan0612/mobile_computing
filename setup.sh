#!/bin/bash

echo "Build and run backend"
docker-compose up --build -d
echo "wait 15s for initial backend"
sleep 15
echo "Insert content in database"
cd script && ./insertDB.sh && cd ..
docker-compose stop

echo "Setup successfully executed"