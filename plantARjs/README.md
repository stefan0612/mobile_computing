# PlantARjs



## Development process

Install dependencies with npm or yarn

```
yarn install
```

Run Application with npm or yarn in your browser:

```
yarn start
```

## Build project for platform

Android Studio must be installed for the android build

Run following command to build platnARjs for android

```
ionic capacitor build android
```

The build artifacts (apk) should be found in android/app/build/outputs/apk

Run following command to build platnARjs for ios (not tested yet)

```
ionic capacitor build ios
```

## Run project on platform

For running the project on android devices please run:

```
ionic capacitor run android
```

Afterwards Android Studio will be opened. Select the emulated or connected android device of your choice and run the application

For running the project on ios devices please run the following (not tested):

```
ionic capacitor run ios
```

Alternatively you can install the plantARjs.apk in the root folder