export class Plant {

    name: string;
    imageURL: string;
    desc: string;

    constructor(name: string, imageURL: string, desc: string) {
        this.name = name;
        this.imageURL = imageURL;
        this.desc = desc;
    }
}
