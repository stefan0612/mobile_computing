import { Component } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Plant} from '../../model/Plant';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  items = [
      new Plant(
          'Bogenhanf',
          'https://cdn02.plentymarkets.com/7bl8tcaea6in/item/images/457/full/BY014856-001-17X60-Zimmerpflanze-Bogenhanf-PS_1.jpg',
          'Der Bogenhanf (Sansevieria) setzt alles daran, die Luftfeuchtigkeit in unserem Zuhause zu regeln, damit Haut, Augen und Atemwege in den Genuss der wohltuenden Wirkung dieser Pflanze kommen'
      ),
      new Plant(
          'Philodendron',
          'https://pikaplant.com/wp-content/uploads/pikaplant-musa-acuminata-coco-fibre-1200x675.jpg',
          'Modell zu klein skaliert! Die Philodendren, zu deutsch Baumfreund, sind die einzige Gattung der Tribus Philodendreae in der Unterfamilie Aroideae innerhalb der Pflanzenfamilie der Aronstabgewächse'
      ),
      new Plant(
          'Strelitzia (Kein Modell vorhanden)',
          'https://www.ikea.com/us/en/images/products/fejka-artificial-potted-plant-with-pot-indoor-outdoor-succulent__0614211_PE686835_S5.JPG',
          'Die Pflanzengattung Strelitzia gehört zur Familie der Strelitziengewächse'
      )
  ];

  constructor(private router: ActivatedRoute) {}

  splitByColumn(column) {
     return column === 1
        ? this.items.slice(0, Math.ceil(this.items.length / 2))
        : this.items.slice(Math.ceil(this.items.length / 2));
  }


}
