import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArLauncherComponent } from './ar-launcher.component';

const routes: Routes = [
    {
        path: '',
        component: ArLauncherComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ArRoutingModule {}
