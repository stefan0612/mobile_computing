import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-ar-launcher',
  templateUrl: './ar-launcher.component.html',
  styleUrls: ['./ar-launcher.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ArLauncherComponent implements OnInit {

  iframeURL: SafeResourceUrl;

  constructor(
      private modalCtrl: ModalController,
      private sanitizer: DomSanitizer,
      private route: ActivatedRoute,
      private router: Router
  ) {}

  ngOnInit() {
    const host = window.location.protocol + window.location.host;
    this.route.params.subscribe(params => {
      const name = params.model;
      const url = this.getGeneratedPageURL(`${host}/assets/${name}/${name}.obj`, `${host}/assets/${name}/${name}.mtl`);
      this.iframeURL = this.sanitizer.bypassSecurityTrustResourceUrl(url);
    });
  }

  getGeneratedPageURL(modelURL: string, mtlURL: string) {
    const source = `
      <!doctype HTML>
      <html>
        <script src="https://aframe.io/releases/0.6.1/aframe.min.js"></script>
        <script src="https://cdn.rawgit.com/jeromeetienne/AR.js/1.5.0/aframe/build/aframe-ar.js"> </script>
        <body style='margin : 0px; overflow: hidden;'>
            <a-scene embedded arjs="debugUIEnabled: false; sourceType: webcam;">
                <a-marker preset="hiro">
                    <a-obj-model scale="0.01 0.01 0.01" position="0 0 0" src="${modelURL}" mtl="${mtlURL}"></a-obj-model>
                </a-marker>
                <a-entity camera></a-entity>
            </a-scene>
        </body>
      </html>
    `;
    return this.getBlobURL(source, 'text/html');
  }

  getBlobURL(code, type) {
    const blob = new Blob([code], {type});
    return URL.createObjectURL(blob);
  }

}
