import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { ArLauncherComponent } from './ar-launcher.component';

import { ArRoutingModule } from './ar-routing.module';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ArRoutingModule
    ],
    declarations: [ArLauncherComponent],
    exports: [
        ArLauncherComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ArModule {}
