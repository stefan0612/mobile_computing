# Plant-AR App Documentation

[TOC]

## Project Structure

```bash
.
├── app         Plant-AR NativeScript App
├── assets      Documentation Pictures
├── backend     Plant-AR Backend Services
├── database    Plant-AR Backend Database
├── plantARjs   AR.js Ionic App 
├── proxy       Reverse-Proxy COnfig for Docker-Compose
├── script      Content for Database
```

## Pre-Requirements

**Nativescript Setup:**
To start deploying nativescript application please setup your environment follwoed by [this link](https://docs.nativescript.org/angular/start/quick-setup#full-setup).

**Android Build-Chain Setup:**

For a simple setup with UI, use Android Studio from [here](https://developer.android.com/studio). Following list shows the needed tools and version for emulator setup:
* Android 10 (API 29) x86
* Android SDK Build Tools Version 30.0.x
* NDK Version 20.0.x (App Build works only with version 20.0)
* Android SDK Command-line Tools 4.0.x
* CMake Version 3.10.2
* Android SDK Platform-Tools Version 30.0.x

**Android Emulator Setup:**

* Android Emulator Version 30.2.6

Setup a new Android Device, for example use the google Pixel 4 XL and config a Virtual Scene as Back Camera during the setup dialog.

![setup device](assets/virtual-device_conf.jpg)

 Start the emulated device and change the OpenGL version to **OpenGL ES 3.1** as shown in the next picture.

![Emulator Config](assets/emulator_config.jpg)

Add the needed Google AR-Service from [here](https://github.com/google-ar/arcore-android-sdk/releases/download/v1.22.0/Google_Play_Services_for_AR_1.22.0_x86_for_emulator.apk) and install it with `adb install Google_Play_Services_for_AR_1.22.0_x86_for_emulator.apk`.

## Standalone Commands

* run setup script only at first run: `./setup.sh`.

* run backend and deploy app on device with: `./run.sh`.

### NativeScript App

To start the app with logging output, run the following commands under folder `app/`.

**Home Network for Wifi usage:** Run as Dev env: `tns run android --env.environment=dev`.
If you want to change the IP-Address the application should use, you have to modify the following files:

* `app/App_Resources/Android/src/main/res/xml/network_security_config.xml` and change backend IP-Address with `<domain includeSubdomains="true">x.x.x.x</domain>`
* `app/src/environments/environment.dev.ts` change the backend IP-Address.

**Local Network for Emulator only:** Run as Prod env: `tns run android --env.environment=prod`.

### Backend and Database

Run backend with `docker-compose up`. Initial content in db (only when backend runs) under `script` run `./insertDB.sh`.

## Architecture

![Component Diagramm](assets/App_Architecture_Mobile-Computing.jpg)

### Service Architecture

![Service Diagram](assets/Mobile_Computing_Service_Architecture.png)

## Technology Stack

**Plant-AR App:**

* [NativeScript](https://nativescript.org/) as Cross-Platform Framework
* [NativeScript-AR](https://github.com/EddyVerbruggen/nativescript-ar) Plugin for ARCore and ARKit usage
* [Adapter-SQLite-NativeScript](https://github.com/only-cliches/Nano-SQL) Plugin for in SQL Database usage
* [NativeScript-Downloader](https://github.com/triniwiz/nativescript-downloader) Plugin for Binary Downloads 
* [Angular](https://angular.io/) as Web-Framework within NativeScript
* [TypeScript](https://www.typescriptlang.org/) as Programming Language in Angular

**Backend:**

* [Ballerina](https://ballerina.io/) as Backend Language
* [MySQL Connector](https://dev.mysql.com/downloads/connector/j/) Library for Database usage

**Database:**

* [MariaDB](https://mariadb.org/) as Database in Docker runtime

**Proxy:**

* [Nignx](https://www.nginx.com/) as Reverse-Proxy

**Local Dev Environment:**

* [Docker](https://docs.docker.com/get-started/overview/) for Backend Services
* [Docker-Compose](https://docs.docker.com/compose/) for Local Runtime
* [Android Studio](https://developer.android.com/studio) for Emulator Setup and Android Build-Chain
* [VCodium](https://vscodium.com/) for Frontend Development
* [IntelliJ IDEA](https://www.jetbrains.com/idea/) for Backend Development

## PlantARjs

The Documentation for ARjs can be found in the [plantARjs folder](plantARjs/)
